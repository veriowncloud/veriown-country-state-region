#!/usr/bin/env node

const { promisify } = require('util');
const path = require('path');
const fs = require('fs').promises;
/** @type {(id: string) => Promise<any>} */
const mkdirp = promisify(require('mkdirp'));
const config = require('config');
const GeonamesCountry = require('../lib/geonames-country');

/** @type {{ username: string }} */
const credentials = config.get('credentials.geonames');

/** @type {string[]} */
const countryCodes = config.get('countriesToFetch');
/** @type {string[]} */
const locales = config.get('locales');
const dataDir = path.resolve(__dirname, '../../data');

const log = (...strings) => {
  // eslint-disable-next-line no-console
  console.log('\x1b[36m%s\x1b[0m', strings.join(' '));
};

/**
 * @typedef Country
 * @property {string} code Country code
 * @property {string} name
 */

/**
 * Write GeonamesCountry instances to given file
 *
 * @param {GeonamesCountry[]} countries Parameter description.
 * @param {string} filepath File to which countries will be written
 * @returns {Promise<void>} Return description.
 */
const writeCountries = async (countries, filepath) => {
  /**
   * Convert {GeonamesCountry} to a JSON {Country} that can be written into JSON file.
   * @param {GeonamesCountry} country
   * @returns {Promise<Country>}
   */
  const writeableCountry = async (country) => {
    await country.fetchInfo(); // eslint-disable-line no-await-in-loop

    return {
      code: country.code,
      name: country.name
    };
  };

  const writeableCountries = await Promise.all(countries.map((c) => writeableCountry(c)));

  log('Writing Country:', writeableCountries.map((c) => c.name));

  await fs.writeFile(filepath, JSON.stringify(writeableCountries, null, '  '));
};

const writeStates = async (countries, stateDir) => {
  await Promise.all(countries.map(async (country) => {
    const statesFilePath = path.resolve(stateDir, `${country.code}.json`);
    const writeableStates = (await country.getStates()).map((s) => {
      return {
        code: s.code,
        name: s.name
      };
    });

    log('\n===\n');
    log('Writing States:\n', writeableStates.map((c) => c.name).join('\n'));
    log('\n===\n');
    await fs.writeFile(statesFilePath, JSON.stringify(writeableStates, null, '  '));
  }));
};

const writeRegions = async (countries, regionDir) => {
  await Promise.all(countries.map(async (country) => {
    country.states.map(async (state) => {
      const regionFilePath = path.resolve(regionDir, `${state.code}.json`);
      const writableRegions = (await country.getRegions(state.code)).map((r) => {
        return {
          code: r.code,
          name: r.name
        };
      });

      log('\n===\n');
      log('Writing Regions:\n', writableRegions.map((c) => c.name).join('\n'));
      log('\n===\n');
      await fs.writeFile(regionFilePath, JSON.stringify(writableRegions, null, '  '));
    });
  }));
};

(async () => {
  /* eslint-disable no-await-in-loop */
  for (const locale of locales) {
    const localeDir = path.resolve(dataDir, locale);
    const stateDir = path.resolve(localeDir, 'states');
    const regionDir = path.resolve(localeDir, 'regions');
    const countriesFilepath = path.resolve(localeDir, 'countries.json');

    await mkdirp(localeDir);
    await mkdirp(stateDir);
    await mkdirp(regionDir);

    const countries = countryCodes.map((code) => new GeonamesCountry({
      username: credentials.username,
      locale,
      code,
      // eslint-disable-next-line global-require, import/no-dynamic-require
      translations: locale === 'en' ? null : require(`../translations/${locale}.json`)
    }));

    await writeCountries(countries, countriesFilepath);

    await writeStates(countries, stateDir);

    await writeRegions(countries, regionDir);
  }
  /* eslint-enable */
})().catch((err) => console.error(err)); // eslint-disable-line no-console
