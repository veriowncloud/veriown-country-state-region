const supportedLocales = ['en', 'hi'];

/**
 * @param {string} locale
 * @returns {boolean}
 */
const isLocaleSupported = (locale) => supportedLocales.indexOf(locale) >= 0;

module.exports = {
  supportedLocales,
  isLocaleSupported
};
