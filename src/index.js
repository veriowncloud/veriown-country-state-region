const { supportedLocales, isLocaleSupported } = require('./locales');

/**
 * @typedef IRegion
 * @property {string} code
 * @property {string} name
 */

/**
 * Get list of countries in given {locale}.
 * @param {string} locale Locale in which country name will be given
 * @returns {Promise<IRegion[]>}
 */
async function getCountries(locale = 'en') {
  try {
    if (!isLocaleSupported(locale)) {
      throw new Error(`InvalidInput: Locale ${locale} is not supported. Supported locales are ${supportedLocales}`);
    }
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const countryData = require(`../data/${locale}/countries.json`);

    return countryData;
  } catch (ex) {
    return ex;
  }
}

/**
 * Get list of States of {country} in given {locale}.
 * @param {string} countryCode
 * @param {string} locale Locale in which names will be given
 * @returns {Promise<IRegion[]>}
 */
async function getStatesForCountry(countryCode = 'in', locale = 'en') {
  try {
    if (!isLocaleSupported(locale)) {
      throw new Error(`InvalidInput: Locale ${locale} is not supported. Supported locales are ${supportedLocales}`);
    }
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const stateData = require(`../data/${locale}/states/${countryCode}.json`);

    return stateData;
  } catch (ex) {
    return ex;
  }
}

/**
 * Get list of States of {stateCode} in given {locale}.
 * @param {string} stateCode
 * @param {string} locale Locale in which names will be given
 * @returns {Promise<IRegion[]>}
 */
async function getRegionsForState(stateCode, locale = 'en') {
  try {
    if (!isLocaleSupported(locale)) {
      throw new Error(`InvalidInput: Locale ${locale} is not supported. Supported locales are ${supportedLocales}`);
    }
    // eslint-disable-next-line global-require,import/no-dynamic-require
    const regionData = require(`../data/${locale}/regions/${stateCode}.json`);

    return regionData;
  } catch (ex) {
    return ex;
  }
}

module.exports = {
  getCountries,
  getStatesForCountry,
  getRegionsForState
};
