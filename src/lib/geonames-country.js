/** @type import('node-fetch').default */
// @ts-ignore
const fetch = require('node-fetch');
/** @type import('slugify').default */
// @ts-ignore
const slugify = require('slugify');

/**
 * @typedef GeonamesInput
 * @property {string} username Username of geonames.org
 * @property {string} locale
 * @property {string} code Country code
 * @property {{[name: string]: string}} [translations]
 */

/**
 * @typedef GeonamesState
 * @property {number} id
 * @property {string} name
 * @property {string} code
 */

/**
 *  GeonamesCountry represent a Country that's connected to Geonames
 */
class GeonamesCountry {
  /**
   * @param {GeonamesInput} input
   */
  constructor({
    username, locale, code, translations
  }) {
    this.username = username;
    this.locale = locale;
    this.code = code;

    /** @type number | null */
    this.id = null;
    /** @type GeonamesState[] | null */
    this.states = null;

    this.translations = this.locale === 'en' ? null : translations;

    if (this.locale !== 'en' && !this.translations) {
      throw new Error(`ArgumentError: Missing translations for locale ${this.locale}`);
    }
  }

  async fetchInfo() {
    const countries = (await fetch(`http://api.geonames.org/countryInfoJSON?country=${this.code.toUpperCase()}&username=${
      this.username
    }&style=full`).then((res) => res.json())).geonames;

    const { id, name } = countries.reduce((_, c) => {
      return { id: c.geonameId, name: c.countryName };
    }, null);

    this.id = id;
    this.name = this.translations ? this.translations[name] : name;
  }

  /**
   * @returns {Promise<GeonamesState[]>}
   */
  async getStates() {
    if (this.states) {
      return this.states;
    }

    if (!this.id) {
      await this.fetchInfo();
    }

    const countryId = this.id;

    const states = await fetch(`http://api.geonames.org/childrenJSON?geonameId=${countryId}&username=${this.username}`).then((res) => res.json());

    this.states = states.geonames.map((state) => {
      const slug = slugify(state.name, { lower: true });
      const name = this.translations ? this.translations[state.name] : state.name;

      if (!name) {
        throw new Error(`TranslationNotFound: State name ${state.name} not found`);
      }

      return {
        name,
        id: state.geonameId,
        code: `${this.code}:${slug}`
      };
    });

    return this.states || []; // keeps type-checker happy
  }

  async getRegions(stateCode) {
    const states = await this.getStates();
    const state = states.find((x) => x.code === stateCode);

    if (!state) {
      throw new Error(`InvalidInput: Couldn't find a state with code: ${stateCode}`);
    }

    const regions = (await fetch(`http://api.geonames.org/childrenJSON?geonameId=${state.id}&username=${this.username}`).then((res) => res.json())).geonames;

    return regions.map((r) => {
      const slug = slugify(r.name, { lower: true });
      const name = this.translations ? this.translations[r.name] : r.name;

      if (!name) {
        throw new Error(`TranslationNotFound: Region name ${r.name} not found`);
      }

      return {
        name,
        id: r.geonameId,
        code: `${state.code}:${slug}`
      };
    });
  }
}

module.exports = GeonamesCountry;
