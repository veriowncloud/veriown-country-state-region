const { describe, it } = require('mocha');
const chai = require('chai');
const getData = require('./index');

const { expect } = chai;

describe('getData', () => {
  describe('.getCountries', () => {
    it('should get countries data', async () => {
      const countriesData = await getData.getCountries();

      expect(countriesData.length).to.be.equal(1);
    });
  });

  describe('.getStates', () => {
    it('should get states data', async () => {
      const stateData = await getData.getStatesForCountry('in');
      expect(stateData.length).to.be.equal(36);
      expect(stateData[0].name).to.be.equal('Andaman and Nicobar');
      expect(stateData[0].code).to.be.equal('in:andaman-and-nicobar');
    });

    it('should get states data for locale: hi', async () => {
      const stateData = await getData.getStatesForCountry('in', 'hi');
      expect(stateData.length).to.be.equal(36);
      expect(stateData[0].name).to.be.equal('अण्डमान और निकोबार');
      expect(stateData[0].code).to.be.equal('in:andaman-and-nicobar');
    });
  });

  describe('.getRegions', () => {
    it('should get regions data', async () => {
      const stateCode = 'in:punjab';
      const regionData = await getData.getRegionsForState(stateCode);
      expect(regionData.length).to.be.equal(22);
      expect(regionData[0].name).to.be.equal('Amritsar');
      expect(regionData[0].code).to.be.equal('in:punjab:amritsar');
    });

    it('should get regions data for locale: hi', async () => {
      const stateCode = 'in:punjab';
      const regionData = await getData.getRegionsForState(stateCode, 'hi');
      expect(regionData.length).to.be.equal(22);
      expect(regionData[0].name).to.be.equal('अमृतसर');
      expect(regionData[0].code).to.be.equal('in:punjab:amritsar');
    });

    it('should fail for invalid stateCode', async () => {
      const stateCode = 'xyz';
      const regionData = await getData.getRegionsForState(stateCode);
      expect(regionData).to.be.an('error');
    });
  });
});
