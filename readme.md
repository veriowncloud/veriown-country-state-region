Installation

```
npm install --save @veriown/country-state-region
```

Usage

1. Import the functions.

```
const { getCountries, getStatesForCountry, getRegionsForState } =  require('@veriown/country-state-region');
```

2. Use wherever you want.

```
getCountries();  
/* output format: [
    {
        "name": "India",
        "geonameId": 1269750,
        "code": "in"
    },
    ...
] */

getStatesForCountry();
/* output format:
[
    {
        "name": "Andaman and Nicobar",
        "geonameId": 1278647,
        "code": "in:andaman-and-nicobar"
    },
    ...
] */    
```	        
        